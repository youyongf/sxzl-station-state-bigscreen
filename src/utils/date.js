export function getCurrentDate(tim) {

    var myDate = new Date(tim);

    if(!tim){myDate = new Date();}
  
    var year = myDate.getFullYear(); //年
  
    var month = myDate.getMonth() + 1; //月
  
    var day = myDate.getDate(); //日
  
    var hour = myDate.getHours(); //时
  
    var min = myDate.getMinutes(); //分
  
    var seconds = myDate.getSeconds(); //秒
  
    // var days = myDate.getDay();
  
    // switch (days) {
  
    //   case 1:
  
    //     days = '星期一';
  
    //     break;
  
    //   case 2:
  
    //     days = '星期二';
  
    //     break;
  
    //   case 3:
  
    //     days = '星期三';
  
    //     break;
  
    //   case 4:
  
    //     days = '星期四';
  
    //     break;
  
    //   case 5:
  
    //     days = '星期五';
  
    //     break;
  
    //   case 6:
  
    //     days = '星期六';
  
    //     break;
  
    //   case 0:
    //     days = '星期日';
  
    //     break;
    // }
  
    var str = year + "-" + zeroFill(month) + "-" + zeroFill(day) + '\xa0' + zeroFill(hour) + ":" + zeroFill(min) + ":" + zeroFill(seconds);
    // var str = year + "/" + zeroFill(month) + "/" + zeroFill(day);
  
    return str;
  
  }
  
  function zeroFill(num) {
    return Number(num) < 10 ? '0' + num : num
  }